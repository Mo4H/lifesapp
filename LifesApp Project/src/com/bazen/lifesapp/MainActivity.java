package com.bazen.lifesapp;

import org.json.JSONArray;

import moah.lib.android.activity.ActivityManager;
import moah.lib.android.activity.MActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bazen.lifesapp.achievements.AchievementManager;
import com.bazen.lifesapp.net.LADataService;
import com.bazen.lifesapp.screens.LAActivityManager;

public class MainActivity extends MActivity {

	// Used for logging and debuging
	public static final String TAG = "LifesApp";
	
	private ListView list;
	private ArrayAdapter<String> adapter;
	private AchievementManager achievementManager = AchievementManager.getInstance();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//achievementManager.fillLibrary();	

		//JSONObject obj = LADataService.instance().checkVersion();
		//Log.d(TAG, obj.toString());
		//JSONArray obj2 = LADataService.instance().getUpdate(); 
		//Log.d(TAG, obj2.toString());
		//Log.e("currentActivity", "" + ActivityManager.getInstance().getCurrentActivity());
		//ActivityManager.getInstance().setCurrentActivity(this);
		ActivityManager.getInstance().startActivity( this, LAActivityManager.ACTIVITY_ACHIEVEMENTS );
		ActivityManager.getInstance().startActivity( this, LAActivityManager.ACTIVITY_PRELOADER ); 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
