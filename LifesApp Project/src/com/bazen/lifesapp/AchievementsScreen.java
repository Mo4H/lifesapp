package com.bazen.lifesapp;

import java.util.ArrayList;

import moah.lib.android.activity.MActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bazen.lifesapp.sqlite.LADatabase;

public class AchievementsScreen extends MActivity{
	
	private ListView _lvAchievements;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.achievement_listlayout);
		
		_lvAchievements = (ListView)findViewById(R.id.lvAchievementList);
		LADatabase entry = new LADatabase(this);
		entry.open();
		ArrayList<String> list = entry.getCategories();
		entry.close();
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
		_lvAchievements.setAdapter(adapter);
	}
}
