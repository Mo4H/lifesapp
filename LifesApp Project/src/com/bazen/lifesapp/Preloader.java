package com.bazen.lifesapp;

import moah.lib.android.activity.ActivityManager;
import moah.lib.android.activity.MActivity;
import moah.lib.android.file.FileHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.bazen.lifesapp.achievements.AchievementManager;
import com.bazen.lifesapp.file.FileNames;
import com.bazen.lifesapp.net.LADataService;
import com.bazen.lifesapp.net.LADataTypes;

public class Preloader extends MActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preloader_layout);
		
		if(LADataService.instance().isConnected(this)){
			checkVersion();
		}else{
			Dialog d = new Dialog(this);
			d.setTitle("Connection");
			TextView tv = new TextView(this);
			tv.setText("Connection failed. For the first time internet is required the get the latest Achievements.");
			d.setContentView(tv);
			d.show();
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return true;
	}
	
	private void checkVersion(){
		JSONObject versionObj = LADataService.instance().checkVersion();
		Log.d("Preloader", versionObj.toString());
		double remoteVersion = 0;
		if(versionObj != null){
			try {
				remoteVersion = versionObj.getDouble(LADataTypes.TAG_VERSION);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Preloader", "Could not get version number");
				
			}
		}
		
		FileHandler fh = new FileHandler(this);
		String file = fh.readFile( FileNames.VERSION_FILE );
		if(file != null){
			double localVersion = Double.parseDouble(file);
			if(remoteVersion > localVersion){
				// Creating the yes no switch.
				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        switch (which){
				        case DialogInterface.BUTTON_POSITIVE:
				        	JSONArray achievements = LADataService.instance().getUpdate();
							if(achievements != null){
								AchievementManager.getInstance().fillDatabase(ActivityManager.getInstance().getCurrentActivity(), achievements);
							}
				            ActivityManager.getInstance().getCurrentActivity().finish();
				            break;

				        case DialogInterface.BUTTON_NEGATIVE:
				            ActivityManager.getInstance().getCurrentActivity().finish();
				            break;
				        }
				    }
				};
				// Create the dialog.
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("New update found. Do you want to update?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
			}else if( remoteVersion == localVersion ){
				ActivityManager.getInstance().getCurrentActivity().finish();
			}
		}else{
			fh.writeFile( FileNames.VERSION_FILE, Double.toString(remoteVersion) );
			JSONArray achievements = LADataService.instance().getUpdate();
			Log.d("Preloader", achievements.toString());
			if(achievements != null){
				AchievementManager.getInstance().fillDatabase( ActivityManager.getInstance().getCurrentActivity(), achievements );
			}
			ActivityManager.getInstance().getCurrentActivity().finish();
		}
	}
}
