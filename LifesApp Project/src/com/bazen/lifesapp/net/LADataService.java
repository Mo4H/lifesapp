package com.bazen.lifesapp.net;

import java.util.ArrayList;

import moah.lib.android.net.DataService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class LADataService extends DataService {
	
	protected static final LADataService LAINSTANCE = new LADataService();
	
	public LADataService(){
		this.setBaseUrl("http://lifesapp.royspringer.nl/lifesApp.php");
	}
	/**
	 * getting the Version Number
	 * @return The version as JSONObject
	 */
	public JSONObject checkVersion(){
		String[] list = {	"tag", 		LADataTypes.TAG_VERSION	};
		return this.callJSONObject(createNameValueList(list));
	}
	/**
	 * Creating a user in the database
	 * @param name		Name
	 * @param email		Email
	 * @param password	Passord not encryted
	 * @return the result of creating the user as JSONObject.
	 */
	public JSONObject createUser(String name, String email, String password){
		String[] list = {	"tag", 		LADataTypes.TAG_CREATE_USER,
							"name",		name,
							"email",	email,
							"password",	password				};
		return this.callJSONObject(createNameValueList(list));
	}
	
	/**
	 * getting the achievements from the database.
	 * @return the achievementTable as JSONObject.
	 */
	public JSONArray getUpdate(){
		String[] list = {	"tag", 		LADataTypes.TAG_UPGRADE	};
		return this.callJSONArray(createNameValueList(list), "achievements");
	}
	
	/**
	 * Creating BasicNameValuePair data object of two Strings
	 * @param tag		the tag send to to url
	 * @param tagName	the tag Name of the tag that will be read by the server;
	 * @return BasicNameValuePair
	 */
	public BasicNameValuePair createTag(String tag, String tagName){
		return new BasicNameValuePair(tag, tagName);
	}
	
	/**
	 * Creating a NameValuePair List based on a String Array
	 * IMPORTANT: MakeSure that the String Array is Even!
	 * @param nameValueList String Array
	 * @return ArrayList<NameValuePair>
	 */
	public ArrayList<NameValuePair> createNameValueList(String[] nameValueList){
		ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
		int listLength = nameValueList.length;
		if((listLength % 2) == 0){
			for (int i = 0; i < listLength; i+=2) {
				list.add(createTag(nameValueList[i], nameValueList[i+1]));
			}
		}else{
			Log.e("LADataService", "Error @ createNameValueList(): Make sure the String Array is EVEN! returning empty ArrayList<NameValuePair>.");
		}
		return list;
	}
	
	public static LADataService instance(){ return LAINSTANCE; }
}
