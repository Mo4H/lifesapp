package com.bazen.lifesapp.screens;

import moah.lib.android.activity.ActivityManager;


public class LAActivityManager extends ActivityManager{

	private static final LAActivityManager LAINSTANCE = new LAActivityManager();

	public static final String ACTIVITY_PRELOADER = "com.bazen.lifesapp.PRELOADER";
	public static final String ACTIVITY_ACHIEVEMENTS = "com.bazen.lifesapp.ACHIEVEMENTSSCREEN";
	public static final String ACTIVITY_ACHIEVEMENT_SINGLE = "com.bazen.lifesapp.SINGLEACHIEVEMENTSCREEN";

	protected LAActivityManager(){
		
	}
	
	public static LAActivityManager instance(){ return LAINSTANCE; }
}

