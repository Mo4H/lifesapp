package com.bazen.lifesapp.achievements;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.bazen.lifesapp.sqlite.LADatabase;

public class AchievementManager {

	private static final AchievementManager INSTANCE = new AchievementManager();
	
	private final AchievementLibrary library = new AchievementLibrary();
	/** @return the library */
	public AchievementLibrary getLibrary() { return library; }

	/**
	 * Constructor
	 */
	private AchievementManager(){
		
	}
	
	public void fillDatabase(Context context, JSONArray jArray){
		List<Achievement> achievements;
		try {
			achievements = convertJSONArray(jArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		LADatabase database = new LADatabase(context);
		database.open();
		for (int i = 0; i < achievements.size(); i++) {
			long row = database.addAchievement(achievements.get(i));
			if(row == -1){
				Log.e("AchievementManager", "Could not add achievement");
			}
		}
		database.close();
	}
	
	public List<Achievement> convertJSONArray(JSONArray jArray) throws JSONException{
		List<Achievement> achievements = new ArrayList<Achievement>();
		for(int i = 0; i < jArray.length(); i++){
	        JSONObject currentObject = jArray.getJSONObject(i);
	        Achievement achievement;
	        // Storing each json item in variable
	        int id = currentObject.getInt("id");
	        String name = currentObject.getString("name");
	        String description = currentObject.getString("description");
	        String difficulty = currentObject.getString("difficulty");
	        String category = currentObject.getString("category");
	        String status = currentObject.getString("status");
	        int maximumProgress = currentObject.getInt("max_progress");
	        int points = currentObject.getInt("points");
	        achievement = new Achievement(id, name, description, difficulty, category, 0, maximumProgress, points, status);
	        achievements.add(achievement);
		}
		return achievements;
	}
	
	public static AchievementManager getInstance(){ return INSTANCE; }
}
