package com.bazen.lifesapp.achievements;

public class Achievement {

	private int id = -1;
	/** @return the Unique id */
	public int getId() { return id; }

	private String name;
	/** @return the name */
	public String getName() { return name; }
	
	private String description;
	/** @return the description */
	public String getDescription() { return description; }
	
	private String difficulty;
	/** @return the type */
	public String getDifficulty() { return difficulty; }
	
	private String category;
	/** @return the category */
	public String getCategory() { return category; }
	
	private String status;
	/** @return the side*/
	public String getStatus() { return status; }
	
	private int progress;
	public void setProgress( int value ) {
		progress += value;
		if (progress < 0) progress = 0;
		if (progress > maximumProgress) progress = maximumProgress;
	}	
	
	/** @return the progress */
	public int getProgress() {
		return progress;
	}
	
	private int maximumProgress;
	/** @return the Maximum progress */
	public int getMaximumProgress() { return maximumProgress; }
	
	private int points;
	/** @return NOTE: the points given by 1 progress */
	public int getPoints() { return points; }
	
	/** @return boolean of the completion */
	public boolean completed() { return progress == maximumProgress; }
	
	/**
	 * Constructor of the achievement. The achievement holds all the data a achievement needs
	 * 
	 * @param name				The name/title of the achievement
	 * @param description 		The description of the achievement
	 * @param difficulty				The type of the achievement EASY, MEDUIM, HARD
	 * @param category			The category of the achievement
	 * @param progress			The progress of the achievement --> 3 / 25
	 * @param maximumProgress	The maximum progress of the achievement 3 / 25 <--
	 * @param points			The points per progress
	 * @param status				The side of the achievement GOOD, BAD
	 */
	public Achievement(int id, String name, String description, String difficulty, String category, int progress, int maximumProgress, int points, String status ) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.difficulty = difficulty;
		this.category = category;
		this.status = status;
		this.maximumProgress = maximumProgress;
		this.setProgress(progress);
		this.points = points;
	}
	
	/** @return the totalPoints ( points * progress )*/
	public int getTotalPoints() {
		int totalPoints = getProgress() * points;
		return totalPoints;
	}
}
