package com.bazen.lifesapp.achievements;

import java.util.*;

public class AchievementLibrary {

	private ArrayList<Achievement> achievements;
	
	/**
	 * Constructor
	 */
	public AchievementLibrary(){
		achievements = new ArrayList<Achievement>();
	}
	
	/**
	 * Adding Achievement By Achievement Object
	 * @param achievement
	 */
	public void addAchievment( Achievement achievement ){
		achievements.add( achievement );
	}
	
	/**
	 * Adding achievement based on the variables
	 * 
	 * @param id				The id of the achievement
 	 * @param name				The name/title of the achievement
	 * @param description 		The description of the achievement
	 * @param type				The type of the achievement EASY, MEDUIM, HARD
	 * @param category			The category of the achievement
	 * @param progress			The progress of the achievement --> 3 / 25
	 * @param maximumProgress	The maximum progress of the achievement 3 / 25 <--
	 * @param points			The points per progress
	 * @param side				The side of the achievement GOOD, BAD
	 */
	public void addAchievment( int id, String name, String description, String category, String type, int progress, int maximumProgress, int points, String side ){
		Achievement achievement = new Achievement(id, name, description, category, type, progress, maximumProgress, points, side);
		achievements.add( achievement );
	}
	
	/**
	 * Returns a achievement by id
	 * @param id the id of the AChievement
	 * @return Achievement
	 */
	public Achievement getAchievement(int id){
		int arrayLenght = achievements.size();
		for(int i = 0; i < arrayLenght; i++){
			Achievement currentAchievement = achievements.get(i);
			if( id == currentAchievement.getId() ){
				return currentAchievement;
			}
		}
		return null;
	}
	
	/**
	 * Returns a achievement by Name
	 * @param name the name of the AChievement
	 * @return Achievement
	 */
	public Achievement getAchievement(String name){
		int arrayLenght = achievements.size();
		for(int i = 0; i < arrayLenght; i++){
			Achievement currentAchievement = achievements.get(i);
			if( name == currentAchievement.getName() ){
				return currentAchievement;
			}
		}
		return null;
	}
	
	/**
	 * Returns all Achievements
	 * @return ArrayList<Achievement>
	 */
	public ArrayList<Achievement> getAchievements() {
		return achievements;
	}
	
	/**
	 * Returns all Achievements by category
	 * @return ArrayList<Achievement>
	 */
	public ArrayList<Achievement> getAchievements(String category) {
		ArrayList<Achievement> newArray = new ArrayList<Achievement>();
		int arrayLenght = achievements.size();
		for(int i = 0; i < arrayLenght; i++){
			Achievement currentAchievement = achievements.get(i);
			if( category == currentAchievement.getCategory() ){
				newArray.add(currentAchievement);
			}
		}
		return newArray;
	}
	
}
