package com.bazen.lifesapp.achievements;

public class AchievementType {
	
	// Statistics
	public static String GOOD = "Good";
	public static String BAD = "Bad";
	public static String LUCKY = "Lucky";
	public static String UNLUCKY = "Unlucky";
	
	// Categories
	public static String CULTURE = "Culture";
	public static String SPORT = "Sport";
	public static String GAME = "Game";
	public static String INFRASTRUCTURE = "Infrastructure";
	public static String TRAVEL = "Travel";
	public static String LIFESTYLE = "Lifestyle";
	
	// Diffecullties
	public static String EASY = "Easy";
	public static String MEDIUM = "Medium";
	public static String HARD = "Hard";
	
}
