package com.bazen.lifesapp.sqlite;

public class DatabaseTypes {
	// Database
	public static final String DATABASE_NAME 			= "lifes_app";
		// Table name
		public static final String TABLE_ACHIEVEMENTS 			= "tb_achievements";
			// Key names
			public static final String KEY_ACHIEVEMENT_ID 			= "a_id";
			public static final String KEY_ACHIEVEMENT_NAME 		= "a_name";
			public static final String KEY_ACHIEVEMENT_DESCRIPTION 	= "a_description";
			public static final String KEY_ACHIEVEMENT_CATEGORY 	= "a_category";
			public static final String KEY_ACHIEVEMENT_DIFFICULTY	= "a_difficulty";
			public static final String KEY_ACHIEVEMENT_STATUS		= "a_status";
			public static final String KEY_ACHIEVEMENT_MAX_PROGRESS = "a_max_progress";
			public static final String KEY_ACHIEVEMENT_POINTS		= "a_points";
			
		// Table name
		public static final String TABLE_ACHIEVEMENT_PROGRESS 	= "tb_progress";
			// Key names
			
		// Table name
		public static final String TABLE_ACHIEVEMENT_COMPLETE 	= "tb_completed";
			// Key names
			
		// Table name
		public static final String TABLE_USER 					= "tb_user";
			// Key names
			
		// Table name
		public static final String TABLE_FRIEND 				= "tb_friends";
			// Key names
			
	
}
