package com.bazen.lifesapp.sqlite;

import java.util.ArrayList;

import moah.lib.android.file.FileHandler;
import moah.lib.android.net.DataTypes;
import moah.lib.android.sqlite.DatabaseHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract.Contacts.Data;
import android.util.Log;

import com.bazen.lifesapp.achievements.Achievement;
import com.bazen.lifesapp.file.FileNames;

public class LADatabase{

	private final Context _context;
	private SQLiteDatabase _dataBase;
	private DatabaseHelper _dbHelper;
	
	public LADatabase( Context context ) {
		_context = context;
		FileHandler fh = new FileHandler(_context);
		String file = fh.readFile( FileNames.VERSION_FILE );
		double localVersion = 0;
		if(file != null){
			localVersion = Double.parseDouble(file);
		}
		_dbHelper = new DatabaseHelper(_context, DatabaseTypes.DATABASE_NAME, (int) localVersion);
		createAchievementTable();
	}

	private void createAchievementTable() {
		String[] achievementColumns = {	DatabaseTypes.KEY_ACHIEVEMENT_ID 			+ " INTEGER PRIMARY KEY",
										DatabaseTypes.KEY_ACHIEVEMENT_NAME 			+ " VARCHAR(32)",
										DatabaseTypes.KEY_ACHIEVEMENT_DESCRIPTION 	+ " VARCHAR(64)",
										DatabaseTypes.KEY_ACHIEVEMENT_CATEGORY 		+ " VARCHAR(32)",
										DatabaseTypes.KEY_ACHIEVEMENT_DIFFICULTY	+ " VARCHAR(6)",
										DatabaseTypes.KEY_ACHIEVEMENT_STATUS 		+ " VARCHAR(10)",
										DatabaseTypes.KEY_ACHIEVEMENT_MAX_PROGRESS 	+ " INTEGER",
										DatabaseTypes.KEY_ACHIEVEMENT_POINTS 		+ " INTEGER"};
		
		_dbHelper.createTabel(DatabaseTypes.TABLE_ACHIEVEMENTS, achievementColumns);
	}
	
	public long addAchievement(Achievement achievement) {
		ContentValues cv = new ContentValues();
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_ID, 			achievement.getId() );
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_NAME, 			achievement.getName());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_DESCRIPTION, 	achievement.getDescription());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_CATEGORY, 		achievement.getCategory());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_DIFFICULTY, 	achievement.getDifficulty());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_STATUS, 		achievement.getStatus());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_MAX_PROGRESS, 	achievement.getMaximumProgress());
		cv.put(DatabaseTypes.KEY_ACHIEVEMENT_POINTS, 		achievement.getPoints());
		long row = _dataBase.insert(DatabaseTypes.TABLE_ACHIEVEMENTS, null, cv);
		
		return row;
	}
	
	public ArrayList<String> getCategories(){
		// Selecting All the categories but no duplicates
		String query = "SELECT DISTINCT " + DatabaseTypes.KEY_ACHIEVEMENT_CATEGORY + " FROM " + DatabaseTypes.TABLE_ACHIEVEMENTS + ";";
		Cursor c = _dataBase.rawQuery(query, null);
		ArrayList<String> result = new ArrayList<String>();
		int index = c.getColumnIndex(DatabaseTypes.KEY_ACHIEVEMENT_CATEGORY);
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result.add( c.getString(index) );
		}
		return result;
	}
	
	public LADatabase open() throws SQLException{		
		_dataBase = _dbHelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		_dbHelper.close();
	}
	
	public void dropDatabase(){
		_dbHelper.dropTable(new String[]{ DatabaseTypes.TABLE_ACHIEVEMENTS } );
	}
}
